package cmd

import (
	"fmt"
	"os/exec"
	"time"

	"github.com/kardianos/service"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cast"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var serviceCmd = &cobra.Command{
	Use:   "service",
	Short: "Install or uninstall the service",
	Run: func(cmd *cobra.Command, args []string) {
		if err := runServiceCmd(viper.GetViper()); err != nil {
			log.Fatal().Err(err).Send()
		}
	},
}

// list of required keys that must be set
var requiredConfig = []string{"sp-cert", "sp-key", "sp-rooturl"}

func init() {
	rootCmd.AddCommand(serviceCmd)

	serviceCmd.Flags().Bool("install", false, "Install service")
	serviceCmd.Flags().Bool("uninstall", false, "Uninstall service")
}

func checkKeys(v *viper.Viper, keys []string) error {
	for _, k := range keys {
		if !v.IsSet(k) {
			return fmt.Errorf("required key %s not set", k)
		}
	}
	return nil
}

func getServiceArgs(v *viper.Viper) []string {
	serviceArgs := make([]string, 0)
	for k, val := range v.AllSettings() {
		// skip install/uninstall
		if k == "install" || k == "uninstall" {
			continue
		}

		// skip when not set
		if !v.IsSet(k) {
			continue
		}

		// otherwise append to list of args
		serviceArgs = append(serviceArgs, k, cast.ToString(val))
	}

	return serviceArgs
}

func initService(v *viper.Viper, name, display, description string) (service.Service, service.Logger, error) {
	// check all required args have been provided
	if err := checkKeys(v, requiredConfig); err != nil {
		return nil, nil, err
	}

	// handle service init
	svcConfig := &service.Config{
		Name:        name,
		DisplayName: display,
		Description: description,
		Arguments:   getServiceArgs(v),
	}

	prg := &program{
		IdpRefreshInterval: v.GetDuration("idp-refresh"),
		CertificateFile:    v.GetString("cert"),
		KeyFile:            v.GetString("key"),
		ListenAddress:      v.GetString("listen"),
	}
	s, err := service.New(prg, svcConfig)
	if err != nil {
		return nil, nil, err
	}

	l, err := s.Logger(nil)
	if err != nil {
		return nil, nil, err
	}

	return s, l, nil
}

func runServiceCmd(v *viper.Viper) error {
	s, _, err := initService(v, serviceName, serviceDisplay, serviceDescription)
	if err != nil {
		return err
	}

	if v.GetBool("uninstall") {
		if err := s.Uninstall(); err != nil {
			return err
		}
		log.Info().Msg("Service uninstalled")

		return nil
	}

	if v.GetBool("install") {
		if err := s.Install(); err != nil {
			return err
		}
		log.Info().Msg("Service installed")

		// set failure options for service on Windows
		serviceRestartTime, _ := time.ParseDuration("1m")
		serviceRestartActions := fmt.Sprintf("restart/%d/restart/%d/restart/%d", serviceRestartTime.Milliseconds(), serviceRestartTime.Milliseconds()*2, serviceRestartTime.Milliseconds()*4)
		cmd := exec.Command("sc.exe", "failure", serviceName, "reset=", "0", "actions=", serviceRestartActions)
		if err := cmd.Run(); err != nil {
			log.Fatal().Err(err).Msg("Error while setting service failure options")
		}

		return nil
	}

	return nil
}
