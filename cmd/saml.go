package cmd

import (
	"bytes"
	"context"
	"crypto/rsa"
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"net/http"
	"net/url"
	"text/template"
	"time"

	"github.com/crewjam/saml"
	"github.com/crewjam/saml/samlsp"
)

func buildMetadata(issuer, endpoint, nameid, certificate string) ([]byte, error) {
	var metadataTemplate = template.Must(template.New("").Parse(`<?xml version="1.0"?>
	<md:EntityDescriptor xmlns:md="urn:oasis:names:tc:SAML:2.0:metadata" validUntil="{{ .ValidUntil }}" entityID="{{ .Issuer }}">
	  <md:IDPSSODescriptor WantAuthnRequestsSigned="false" protocolSupportEnumeration="urn:oasis:names:tc:SAML:2.0:protocol">
		<md:KeyDescriptor use="signing">
		  <ds:KeyInfo xmlns:ds="http://www.w3.org/2000/09/xmldsig#">
			<ds:X509Data>
			  <ds:X509Certificate>{{ .Certificate }}</ds:X509Certificate>
			</ds:X509Data>
		  </ds:KeyInfo>
		</md:KeyDescriptor>
		<md:KeyDescriptor use="encryption">
		  <ds:KeyInfo xmlns:ds="http://www.w3.org/2000/09/xmldsig#">
			<ds:X509Data>
			  <ds:X509Certificate>{{ .Certificate }}</ds:X509Certificate>
			</ds:X509Data>
		  </ds:KeyInfo>
		</md:KeyDescriptor>
		<md:NameIDFormat>urn:oasis:names:tc:SAML:1.1:nameid-format:{{ .NameId }}</md:NameIDFormat>
		<md:SingleSignOnService Binding="urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect" Location="{{ .Endpoint }}"/>
	  </md:IDPSSODescriptor>
	</md:EntityDescriptor>`))

	buf := new(bytes.Buffer)

	switch nameid {
	case "persistent", "transient", "kerberos", "entity":
		data := struct {
			Issuer, Endpoint, NameId, Certificate, ValidUntil string
		}{
			issuer, endpoint, nameid, certificate, time.Now().AddDate(0, 3, 0).Format(time.RFC3339),
		}
		if err := metadataTemplate.Execute(buf, data); err != nil {
			return nil, err
		}
	default:
		return nil, fmt.Errorf("Invalid SAML 2.0 NameId Format specified: %s", nameid)
	}

	return buf.Bytes(), nil
}

type SamlSpCustom struct {
	Issuer, Endpoint, NameId, Certificate string
}

func NewSamlSp(cert, key, rootUrl string, options interface{}) (*samlsp.Middleware, error) {
	var (
		idpMetadata *saml.EntityDescriptor
		err         error
	)

	// parse certificate and key files
	keyPair, err := tls.LoadX509KeyPair(cert, key)
	if err != nil {
		return nil, err
	}
	keyPair.Leaf, err = x509.ParseCertificate(keyPair.Certificate[0])
	if err != nil {
		return nil, err
	}

	// populate metadata either from a metadata URL or from custom values
	switch options := options.(type) {
	case string:
		idpMetadataURL, err := url.Parse(options)
		if err != nil {
			return nil, fmt.Errorf("metadata URL error: %w", err)
		}

		idpMetadata, err = samlsp.FetchMetadata(context.Background(), http.DefaultClient, *idpMetadataURL)
		if err != nil {
			return nil, fmt.Errorf("metadata fetch error: %w", err)
		}

	case SamlSpCustom:
		b, err := buildMetadata(options.Issuer, options.Endpoint, options.NameId, options.Certificate)
		if err != nil {
			return nil, fmt.Errorf("metadata build error: %w", err)
		}

		idpMetadata, err = samlsp.ParseMetadata(b)
		if err != nil {
			return nil, fmt.Errorf("custom metadata error: %w", err)
		}
	default:
		return nil, fmt.Errorf("Invalid SAML Service Provider options")
	}

	// validate root URL
	rootURL, err := url.Parse(rootUrl)
	if err != nil {
		return nil, err
	}

	return samlsp.New(samlsp.Options{
		URL:               *rootURL,
		Key:               keyPair.PrivateKey.(*rsa.PrivateKey),
		Certificate:       keyPair.Leaf,
		IDPMetadata:       idpMetadata,
		AllowIDPInitiated: true,
	})
}
