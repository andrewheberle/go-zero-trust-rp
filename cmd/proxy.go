package cmd

import (
	"crypto/tls"
	"crypto/x509"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httputil"
	"net/url"
	"strings"
	"time"

	"github.com/cloudflare/backoff"
	"github.com/crewjam/saml/samlsp"
	"github.com/lestrrat-go/jwx/jwa"
	"github.com/lestrrat-go/jwx/jwk"
	"github.com/lestrrat-go/jwx/jwt"
	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
	"gitlab.com/andrewheberle/routerswapper"
)

func singleJoiningSlash(a, b string) string {
	aslash := strings.HasSuffix(a, "/")
	bslash := strings.HasPrefix(b, "/")
	switch {
	case aslash && bslash:
		return a + b[1:]
	case !aslash && !bslash:
		return a + "/" + b
	}
	return a + b
}

func joinURLPath(a, b *url.URL) (path, rawpath string) {
	if a.RawPath == "" && b.RawPath == "" {
		return singleJoiningSlash(a.Path, b.Path), ""
	}
	// Same as singleJoiningSlash, but uses EscapedPath to determine
	// whether a slash should be added
	apath := a.EscapedPath()
	bpath := b.EscapedPath()

	aslash := strings.HasSuffix(apath, "/")
	bslash := strings.HasPrefix(bpath, "/")

	switch {
	case aslash && bslash:
		return a.Path + b.Path[1:], apath + bpath[1:]
	case !aslash && !bslash:
		return a.Path + "/" + b.Path, apath + "/" + bpath
	}
	return a.Path + b.Path, apath + bpath
}

// NewProxy takes target host and creates a simple reverse proxy
func NewProxy(targetHost string, jwks *Jwks) (*httputil.ReverseProxy, error) {
	target, err := url.Parse(targetHost)
	if err != nil {
		return nil, err
	}

	// this is a modified version of httputil.NewSingleHostReverseProxy
	targetQuery := target.RawQuery
	director := func(req *http.Request) {
		req.URL.Scheme = target.Scheme
		req.URL.Host = target.Host
		req.URL.Path, req.URL.RawPath = joinURLPath(target, req.URL)
		if targetQuery == "" || req.URL.RawQuery == "" {
			req.URL.RawQuery = targetQuery + req.URL.RawQuery
		} else {
			req.URL.RawQuery = targetQuery + "&" + req.URL.RawQuery
		}
		if _, ok := req.Header["User-Agent"]; !ok {
			// explicitly disable User-Agent so it's not set to default value
			req.Header.Set("User-Agent", "")
		}

		// start of ensuring headers do not exist
		req.Header.Del(viper.GetString("id-header"))
		req.Header.Del(viper.GetString("jwt-header"))

		// add user identity to HTTP header (or delete to ensure this cannot be spoofed)
		if id := samlsp.AttributeFromContext(req.Context(), viper.GetString("id-attribute")); id != "" {
			req.Header.Add(viper.GetString("id-header"), id)

			// generate token for jwt
			token := jwt.New()
			token.Set(jwt.IssuerKey, viper.GetString("sp-rooturl"))
			if viper.IsSet("jwt-audience") {
				token.Set(jwt.AudienceKey, viper.GetString("jwt-audience"))
			}
			token.Set(jwt.IssuedAtKey, time.Now().Unix())
			token.Set(jwt.ExpirationKey, time.Now().Add(time.Hour).Unix())
			token.Set(viper.GetString("id-attribute"), id)

			// add any additional claims
			if viper.IsSet("jwt-claims") {
				for _, claim := range viper.GetStringSlice("jwt-claims") {
					if val := samlsp.AttributeFromContext(req.Context(), claim); val != "" {
						// add to token
						token.Set(claim, val)
					}
				}
			}

			// sign and serialise token using jwks
			serialised, err := jwt.Sign(token, jwa.SignatureAlgorithm(jwks.Algorithm()), jwks.Key())
			if err == nil {
				// add jwt header
				req.Header.Add(viper.GetString("jwt-header"), string(serialised))
			}
		}

	}

	return &httputil.ReverseProxy{Director: director}, nil
}

type Jwks struct {
	key  jwk.Key
	pub  jwk.Key
	algo string
	set  jwk.Set
}

func (jwks Jwks) Key() jwk.Key {
	return jwks.key
}

func (jwks Jwks) Algorithm() string {
	return jwks.algo
}

func (jwks Jwks) Set() jwk.Set {
	return jwks.set
}

func (jwks Jwks) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	encoder := json.NewEncoder(w)
	if err := encoder.Encode(jwks.Set()); err != nil {
		http.Error(w, "500 Internal Server Error", http.StatusInternalServerError)
		return
	}
}

func NewJwks(cert, key, algo string) (*Jwks, error) {
	// parse certificate and key files
	keyPair, err := tls.LoadX509KeyPair(cert, key)
	if err != nil {
		return nil, err
	}
	keyPair.Leaf, err = x509.ParseCertificate(keyPair.Certificate[0])
	if err != nil {
		return nil, err
	}

	k, err := jwk.New(keyPair.PrivateKey)
	if err != nil {
		return nil, err
	}
	if err := jwk.AssignKeyID(k); err != nil {
		return nil, err
	}

	pub, err := k.PublicKey()
	if err != nil {
		return nil, err
	}

	set := jwk.NewSet()
	set.Add(pub)

	return &Jwks{set: set, key: k, pub: pub, algo: algo}, nil

}

func NewMux() (*http.ServeMux, error) {
	var samlSP *samlsp.Middleware

	// set up jwks handler
	jwks, err := NewJwks(viper.GetString("sp-cert"), viper.GetString("sp-key"), viper.GetString("jwt-algorithm"))
	if err != nil {
		return nil, fmt.Errorf("Error setting up JWKS handler: %w", err)
	}

	// initialise a reverse proxy for the provided target
	proxy, err := NewProxy(viper.GetString("target"), jwks)
	if err != nil {
		return nil, fmt.Errorf("Error setting up Reverse Proxy handler: %w", err)
	}

	// initialise new saml service provider
	if viper.IsSet("idp-metadata") {
		samlSP, err = NewSamlSp(viper.GetString("sp-cert"), viper.GetString("sp-key"), viper.GetString("sp-rooturl"), viper.GetString("idp-metadata"))
	} else {
		samlSP, err = NewSamlSp(viper.GetString("sp-cert"), viper.GetString("sp-key"), viper.GetString("sp-rooturl"), SamlSpCustom{viper.GetString("idp-entityid"), viper.GetString("idp-ssourl"), viper.GetString("idp-nameid-format"), viper.GetString("idp-cert")})
	}
	if err != nil {
		return nil, fmt.Errorf("Error setting up SAML SP handler: %w", err)
	}

	// build mux
	mux := http.NewServeMux()
	mux.Handle("/", samlSP.RequireAccount(proxy))
	mux.Handle(viper.GetString("sp-path"), samlSP)
	mux.Handle(viper.GetString("jwks-path"), jwks)

	return mux, nil
}

func refreshRouter(rs *routerswapper.RouterSwapper, max, interval time.Duration) {
	var mux *http.ServeMux
	var err error

	// start our backoff timer
	b := backoff.New(max, interval)
	for {
		// recreate mux
		mux, err = NewMux()
		if err == nil {
			break
		}
		log.Error().Err(err).Msg("Error during refresh")

		// wait for our retry time and re-attempt
		<-time.After(b.Duration())
	}

	// switch to new mux
	rs.Swap(mux)
}
