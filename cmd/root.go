package cmd

import (
	"context"
	"crypto/tls"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/cloudflare/certinel/fswatcher"
	"github.com/kardianos/service"
	"github.com/lestrrat-go/jwx/jwa"
	"github.com/oklog/run"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/andrewheberle/routerswapper"
)

type program struct {
	IdpRefreshInterval time.Duration
	CertificateFile    string
	KeyFile            string
	ListenAddress      string
}

var (
	logger  service.Logger
	rootCmd = &cobra.Command{
		Use:   serviceName,
		Short: serviceDescription,
		Run: func(cmd *cobra.Command, args []string) {
			if err := runRootCmd(viper.GetViper()); err != nil {
				log.Fatal().Err(err).Send()
			}
		},
	}
	serviceName        = "go-zero-trust-rp"
	serviceDisplay     = "Zero Trust Reverse Proxy"
	serviceDescription = "This service enforces Zero Trust access for a local HTTP/HTTPS service."
)

func init() {
	// logging config
	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix

	// set up viper init function
	cobra.OnInitialize(initViperConfig)

	// general command line flags
	rootCmd.PersistentFlags().String("config", "", "Config file")
	rootCmd.PersistentFlags().StringP("listen", "l", ":8000", "Listen address")
	rootCmd.PersistentFlags().StringP("target", "t", "http://localhost:8080", "Proxy target host")
	rootCmd.PersistentFlags().StringP("cert", "c", "", "HTTPS TLS certificate")
	rootCmd.PersistentFlags().StringP("key", "k", "", "HTTPS TLS key")
	rootCmd.PersistentFlags().String("id-attribute", "email", "Session attribute to retrieve user identity from")
	rootCmd.PersistentFlags().String("id-header", "X-Auth-User", "HTTP Header to use to pass user identity to service")
	rootCmd.PersistentFlags().String("jwt-header", "X-Auth-Jwt", "HTTP Header to use to pass JSON Web Token to service")
	rootCmd.PersistentFlags().String("jwt-audience", "", "Audience value for JSON Web Token")
	rootCmd.PersistentFlags().StringSlice("jwt-claims", []string{}, "Additional claims to include in JWT")
	rootCmd.PersistentFlags().String("jwt-algorithm", string(jwa.RS256), "Algorithm for JWT")
	rootCmd.PersistentFlags().String("jwks-path", "/.well-known/jwks.json", "JSON Web Key Set path")

	// service provider flags
	rootCmd.PersistentFlags().String("sp-cert", "", "SAML SP certificate")
	rootCmd.PersistentFlags().String("sp-key", "", "SAML SP key")
	rootCmd.PersistentFlags().String("sp-path", "/saml/", "Path for SAML endpoints")
	rootCmd.PersistentFlags().String("sp-rooturl", "", "SAML SP Root URL")

	// identity provider flags
	rootCmd.PersistentFlags().Duration("idp-refresh", time.Hour*72, "Interval to refresh SAML IdP metadata (0 to disable)")
	rootCmd.PersistentFlags().String("idp-metadata", "", "SAML IdP Metadata URL")
	rootCmd.PersistentFlags().String("idp-ssourl", "", "SAML IdP SSO URL")
	rootCmd.PersistentFlags().String("idp-cert", "", "SAML IdP SSO Certificate (PEM encoded string)")
	rootCmd.PersistentFlags().String("idp-entityid", "", "SAML IdP Entity ID")
	rootCmd.PersistentFlags().String("idp-nameid-format", "persistent", "SAML IdP Name ID Format")

	// bind flags to viper
	_ = viper.BindPFlags(rootCmd.PersistentFlags())
}

func initViperConfig() {
	viper.SetEnvPrefix("rp")
	viper.SetEnvKeyReplacer(strings.NewReplacer("-", "_"))
	viper.AutomaticEnv()

	// load config if it exists
	if viper.IsSet("config") {
		viper.SetConfigFile(viper.GetString("config"))
	} else {
		// if no config was specified then use auto-detection
		viper.SetConfigName("config")
		viper.SetConfigType("yaml")
		viper.AddConfigPath(fmt.Sprintf("/etc/%s/", serviceName))
		viper.AddConfigPath(fmt.Sprintf("$HOME/.%s", serviceName))
		viper.AddConfigPath(".")
	}
	// attempt to read in config but without this being a fatal error
	if err := viper.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			log.Warn().Msg("no config file found")
		} else {
			log.Fatal().Err(err).Msg("config could not be loaded")
		}
	}
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		log.Fatal().Err(err).Send()
	}
}

func runRootCmd(v *viper.Viper) error {
	var err error
	var s service.Service

	s, logger, err = initService(v, serviceName, serviceDisplay, serviceDescription)
	if err != nil {
		log.Fatal().Err(err).Msg("Error initialising service")
	}

	return s.Run()
}

func (p *program) Start(s service.Service) error {
	go p.run()
	return nil
}

func (p *program) Stop(s service.Service) error {
	return nil
}

func (p *program) run() {
	var mux *http.ServeMux
	var err error

	// initialise new saml service provider
	mux, err = NewMux()
	if err != nil {
		log.Fatal().Err(err).Send()
	}

	// allow swapping of routes
	rs := routerswapper.New(mux)

	// refresh IdP metadata on schedule
	if p.IdpRefreshInterval != 0 {
		go func() {
			for {
				// wait for refresh timeout
				time.Sleep(p.IdpRefreshInterval)

				refreshRouter(rs, p.IdpRefreshInterval/2, 0)
			}
		}()
	}

	// custom server
	srv := &http.Server{
		Addr:         p.ListenAddress,
		Handler:      rs,
		ReadTimeout:  60 * time.Second,
		WriteTimeout: 60 * time.Second,
	}

	// start http server
	if p.CertificateFile != "" && p.KeyFile != "" {
		ctx, cancel := context.WithCancel(context.Background())

		// set up watcher to handle certificate changes
		certinel, err := fswatcher.New(p.CertificateFile, p.KeyFile)
		if err != nil {
			log.Fatal().Err(err).Msg("unable to read server certificate")
		}

		g := run.Group{}
		{
			g.Add(func() error {
				return certinel.Start(ctx)
			}, func(err error) {
				cancel()
			})
		}
		{
			// set TLS config to use certinel
			srv.TLSConfig = &tls.Config{
				GetCertificate: certinel.GetCertificate,
			}
			g.Add(func() error {
				return srv.ListenAndServeTLS("", "")
			}, func(err error) {
				ctx, cancel := context.WithTimeout(ctx, 30*time.Second)
				srv.Shutdown(ctx)
				cancel()
			})
		}

		// run our concurrent functions
		if err := g.Run(); err != nil {
			log.Fatal().Err(err).Send()
		}
	} else {
		if err := srv.ListenAndServe(); err != nil {
			log.Fatal().Err(err).Send()
		}
	}
}
