# Zero Trust Reverse Proxy

This a Go based service that is designed to allow "Zero Trust" to be applied to on-premise HTTP applications.

This is achievied by having this service proxy those connections and forcing a SAML 2.0 based sign-in flow for the proxied application.

## Requirements

This proxy requires a SAML 2.0 Identity Provider (IdP) in order to authenticate and authorize users, examples are:

* Cloudflare Access
* Auth0
* SimpleSamlPHP
* ADFS
* Azure AD

## Installation

Assuming you have a functional Go environment you can run the following to download and compile the service:

```sh
go get -u gitlab.com/andrewheberle/go-zero-trust-rp
```

## Usage

Assuming a web service running locally on the host `service.example.net` that listens on `localhost:8080` the Zero Trust proxy can be deployed as follows:

```sh
go-zero-trust-rp \
    --target http://localhost:8080 \
    --listen :443 \
    --cert server.crt \
    --key server.key \
    --sp-rooturl https://service.example.net \
    --sp-cert saml.crt \
    --sp-key saml.key \
    --idp-metadata https://samltest.id/saml/idp
```

After the above command is executed the proxy will spawn a HTTPS server listening on port 443 with SAML 2.0 metadata available from `https://service.example.net/saml/metadata`.

### Identity Provider Configuration

The configuration of specific IdP's varies however all have similar requirements as follows:

| Setting                              | Value                                                   |
|--------------------------------------|---------------------------------------------------------|
| Metadata URL                         | Use the value of: `sp-root-url` + `sp-path` + /metadata |
| Assertion Consumer Service (ACS) URL | `sp-root-url` + `sp-path` + /acs                        |
| Entity ID                            | Same as "Metadata URL"                                  |

Using the samltest.id example above the only requirement on the IdP side is to provide the SP metdata from: `https://service.example.net/saml/metadata`

For an alternative IdP that requires manual configuration, the values would be:

| Setting                              | Value                                                   |
|--------------------------------------|---------------------------------------------------------|
| Metadata URL                         | `https://service.example.net/saml/metadata`             |
| Assertion Consumer Service (ACS) URL | `https://service.example.net/saml/acs`                  |
| Entity ID                            | `https://service.example.net/saml/metadata`             |

### Authentication Flow

The following process describes a new connection to your application:

1. Client connects to `https://service.example.net`
1. Access is checked and the connection is proxied to the application or the rest of the flow is completed
1. Browser is redirected to Identity Provider (IdP) for Authentication and Authorization
1. After Authentication browser is redirected back to Service Provider (SP)
1. Browser is redirected back to original location and the request is proxied to the application

During the above process, the proxy delegates all Authentication and Authorization processes to the IdP and only validates Access in the Authentication flow.

## Running As A Service

To install as a service on Windows:

```sh
go-zero-trust-rp service --install OTHER COMMAND LINE OPTIONS
```

The above should also work on various Linux distributions, however this has not been tested.

## Docker

The service can also be run as a container using Docker:

```sh
docker run \
    -v /path/to/keys:/app/keys \
    -e RP_IDP_METADATA=https://samltest.id/saml/idp \
    -e RP_SP_ROOTURL=http://localhost:8000 \
    -e RP_SP_CERT=/app/keys/saml.crt \
    -e RP_SP_KEY=/app/keys/saml.key \
    -e RP_TARGET=http://app:8080 \
    -p 8000:8000 \
    registry.gitlab.com/andrewheberle/go-zero-trust-rp:latest
```

The above example assumes another service is reachable at `http://app:8080` via some means (ie a container network).

Note: In production it is recommended to use a specific image tag rather than latest. 
## Command Line Options

### General Options

* cert : HTTPS TLS certificate
* key : HTTPS TLS key
* target : Proxy target host (default "http://localhost:8080")
* listen : Listen address (default ":8000")
* id-attribute : Session attribute to retrieve user identity from (default "email")
* id-header : HTTP Header to use to pass user identity to service (default "X-Auth-User")
* config : Path to a YAML formated configuration file (will also check "/etc/go-zero-trust-rp", "$HOME/.go-zero-trust-rp" and ".")
* jwt-algorithm : Algorithm for JWT (default "RS256")
* jwt-audience : Audience value to include in JSON Web Token (JWT) to service
* jwt-claims : Additional claims provided by the SAML IdP to include in JWT
* jwt-header : HTTP Header to use to pass JSON Web Token (JWT) to service (default "X-Auth-Jwt")
* jwks-path : Path for JSON Web Key Set (JWKS) URL (default "/.well-known/jwks.json")

If both `--cert` and `--key` are provided the service will respond to HTTPS connections, otherwise plain HTTP is used.

The service supports changes to the provided TLS certificate and key without restarting to allow certificate renewals and/or key rotation.

This key-pair may be from a public or internal CA or may also be self signed, however to avoid problems during the authentication flow the certificate should be trusted by your clients.

### IdP Options

The SAML IdP must be specified in one of two ways:

1. By providing a metadata URL using the `--idp-metadata` option
2. Explicitly via the `--idp-cert`, `--idp-entityid`, `--idp-nameid-format` and `--idp-ssourl` options

* idp-metadata : SAML IdP Metadata URL
* idp-cert : SAML IdP SSO Certificate (PEM encoded string)
* idp-entityid : SAML IdP Entity ID
* idp-nameid-format : SAML IdP Name ID Format (default "persistent")
* idp-ssourl : SAML IdP SSO URL
* idp-refresh : Interval to refresh SAML IdP metadata (default 72-hours or 0 to disable)"

### Service Options

* install : Install Service (must be the first command line argument)
* uninstall : Uninstall Service

### SP Options

All SP options are mandatory, however `--sp-path` has a default set that should be fine for most situations.

* sp-cert : SAML SP certificate
* sp-key : SAML SP key. This key is also used to sign the JWT provided to the service.
* sp-path : Path for SAML endpoints (default "/saml/")
* sp-rooturl : SAML SP Root URL

The SP certificates can (and probably should) be self signed as follows:

```sh
openssl req -x509 -newkey rsa:2048 -keyout saml.key -out saml.crt -days 365 -nodes -subj "/CN=service.example.net"
```
