FROM golang:1.19 AS builder

COPY . /build

RUN cd /build && \
    CGO_ENABLED=0 go build -o /build/go-zero-trust-rp .

FROM gcr.io/distroless/static-debian10

COPY --from=builder /build/go-zero-trust-rp /app/go-zero-trust-rp

EXPOSE 8000

WORKDIR /app

ENTRYPOINT [ "/app/go-zero-trust-rp" ]
